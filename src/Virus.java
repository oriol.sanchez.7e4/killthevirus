import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.net.URL;
import java.util.Random;


/**
 * Classe bàsica Pilota. Versió Thread per evitar esperes actives de les GUIs
 * Una pilota que es mou i rebota dintre d'uns límits
 * En aquest exemple, els límits venen donats per un panell (JPanel) contenidor
 *
 * @author Montse
 * @version 14/10/2021
 */
public class Virus extends Thread{

    private double x = randomNumber("x"); //perquè la funció ellipse utilitza double
    private double y = randomNumber("y");
    private double dx = randomVelocitat();
    private double dy = randomVelocitat();
    private int radi = 25;
    private boolean alive = true;
    private final URL death_url = getClass().getClassLoader().getResource("imatges/virus_death.jpg");
    private final URL url = getClass().getClassLoader().getResource("imatges/virus.jpg");
    private Image image = new ImageIcon(url).getImage();


    /**
     * Numero aleatori de la pantalla separat per coordenadas
     * @param par paramatre introduit que volem calcular. Haura de ser o "x" o "y"
     * @return retorna un valor que serà utilitzat per inicialitzar el virus en una coordenada concreta
     */
    private int randomNumber(String par){
        Random rn = new Random();
        int number = 0;
        if (par.equals("x")){
            number = rn.nextInt(550 + 1);
        } else if (par.equals("y")){
            number = rn.nextInt(370 + 1);
        }
        return number;
    }

    /**
     * Numero aleatori de la pantalla calcula velocitat del virus
     * @return Retorna valor inferior a 1 en nombre decimal
     */
    private double randomVelocitat(){
        Random rn = new Random();
        return rn.nextDouble();
    }

    private JPanel elPanelComu; //el panell on reboten les pilotes

    public void setElPanelComu(JPanel p) {
        elPanelComu = p;
    }

    /* es desplaça la pilota 1 posició en x i en y*/
    /* quan arriba als limits, canvia de direcció */
    public void mourePilota() {
        Rectangle2D limits = elPanelComu.getBounds();
        double width = limits.getWidth();
        double height = limits.getHeight();
        x += dx;
        y += dy;
        if (x + radi > width || x < 0) dx = -dx;
        if (y + radi > height || y < 0) dy = -dy;
    }

//    public Ellipse2D dibuixarPilota() {
//        return new Ellipse2D.Double(x, y, radi, radi);
//    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public void setDx(double dx) {
        this.dx = dx;
    }
    public void setDy(double dy) {
        this.dy = dy;
    }
    public int getRadi() {
        return radi;
    }
    public void setAlive(boolean alive){
        this.alive = alive;
    }

    /**
     * Canvia a false el estat de vida del virus, i el deten. Per observa que esta mort, li canviem la imatge
     */
    public void matar(){
        setAlive(false);
        setDx(0);
        setDy(0);
        setImage(new ImageIcon(death_url).getImage());
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * Mou el virus a traves de la pantalla
     */
    @Override
    public void run() {
        while (true){
            mourePilota();
            try {
                Thread.sleep(4);
            } catch (Exception e) {
            }
            elPanelComu.repaint();
        }
    }
}