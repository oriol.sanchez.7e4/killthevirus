

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;

/** El panell s'encarrega de crear les pilotes, afegir-les a una llista i pintar-les
 * al mètode paint()
 * @author Montse
 * @version 14/10/2021
 */
class PanelVirus extends JPanel  implements MouseListener {
    private ArrayList<Virus> virus = new ArrayList<Virus>();

    //per fer x i y accessibles des del paint()
    private int x, y;

    public PanelVirus() {
        addMouseListener(this);
    }

    /**
     * Inicia el virus amb un nombre aleatori de bacteries.
     */
    public void addVirus() {
        Random rn = new Random();

        int numVirus = rn.nextInt(10 - 5 + 1) + 5;

        System.out.println(numVirus);

        for (int i = 0; i<numVirus; i++){
            //es crea una pilota, ara és un thread
            Virus v = new Virus();
            //se li passa el panell, per saber les dimensions on ha de rebotar
            v.setElPanelComu(this);
            //s'afegeix a una llista de pilotes, per quan hi hagi vàries
            virus.add(v);
            //es posa en marxa el thread
            v.start();
        }
    }

    /** Mètode que no es pot canviar de nom. Quan es vol pintar un objecte gràfic sobre un Component
     * de Swing de Java.
     * @param g de tipus Graphics
     */
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        /* itera i dibuixa totes les pilotes*/
        for (Virus v : virus) {
            g2.drawImage(v.getImage(), (int) v.getX(), (int) v.getY(), v.getRadi(), v.getRadi(), this);
        }
        //Tip per assegurar que el Graphics s'actualitza
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    /**
     * Quan fem un click printa les coordenades. I en cas de que hi hagi un virus, també el pararà i farà el canvi de imatge
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        x = e.getX();
        y = e.getY();
        //printa per consola les coordenades del mouse en format (x,y)
        System.out.println("(" + x + "," + y + ")");
        repaint(); //es crida a paint()

        for (Virus v : virus) { // Si cliques al rang del virus, s'elimina
            if (x < v.getX()+v.getRadi()
                    && x > v.getX()
                    && y < v.getY()+v.getRadi()
                    && y > v.getY()) {
                v.matar();
            }
        }


    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}

